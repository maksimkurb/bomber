import * as express from "express";
import * as session from "express-session";
import * as http from "http";
import * as SocketIO from "socket.io";

import * as config from "./config";
import { gameHandlers } from "./handlers/gameHandlers";
import { utilHandlers } from "./handlers/utilHandlers";
import { configHandlers } from "./handlers/configHandlers";
import { staticsDevRouter } from "./routes/statics-dev-router";
import { staticsRouter } from "./routes/statics-router";

const app = express();
const server = new http.Server(app);
const io = SocketIO(server);

const sessionMiddleware = session({
  secret: config.SESSION_SECRET
});

app.use(sessionMiddleware);
app.use(config.IS_PRODUCTION ? staticsRouter() : staticsDevRouter());

io.use((socket, next) =>
  sessionMiddleware(socket.request, socket.request.res, next)
);

io.use((socket, next) => {
  socket.request.context = {};
  next();
});

io.on("connection", socket => {
  gameHandlers(socket);
  configHandlers(socket);
  utilHandlers(socket);
});

server.listen(config.SERVER_PORT, () => {
  console.log(`App listening on port ${config.SERVER_PORT}!`);
});
