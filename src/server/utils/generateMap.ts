import { WallType } from "../../shared/WallType";

export const generateMap = (sizeX, sizeY, thereshold = 0.45) => {
  const map = [];
  // Каждый нечётный блок является твёрдой стеной,
  // остальные по рандому, кроме трёх угловых
  for (let i = 0; i < sizeX; i++) {
    const row = [];
    for (let j = 0; j < sizeY; j++) {
      if (i % 2 === 1 && j % 2 === 1) {
        // Проверка на каждый нечётный блок
        row.push(WallType.BLOCK_SOLID);
      } else if (
        // Проверка на угловые блоки
        i + j < 2 || // Left top
        (j < 2 && i > sizeX + j - 3) || // Left bottom
        (i < 2 && j > sizeY + i - 3) || // Right top
        i + j > sizeX + sizeY - 4 // Right bottom
      ) {
        row.push(WallType.BLOCK_NONE);
      } else {
        // Немного рандома
        row.push(
          Math.random() < thereshold
            ? WallType.BLOCK_FRAGILE
            : WallType.BLOCK_NONE
        );
      }
    }
    map.push(row);
  }
  return map;
};
