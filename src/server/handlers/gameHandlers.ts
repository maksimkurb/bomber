import { Socket } from "socket.io";

import { IGameDTO } from "./../../shared/IGameDTO.d";
import { ErrorCode } from "./../../shared/ErrorCode";
import { IErrorDTO } from "./../../shared/IErrorDTO.d";
import { IActionDTO } from "../../shared/IActionDTO";
import { ISetGameDTO } from "../../shared/ISetGameDTO";

import { DEFAULT_SIZE_X, DEFAULT_SIZE_Y } from "../constants";
import { generateMap } from "./../utils/generateMap";
import { gameNextId } from "./../database/index";

import { games } from "../database";

export const gameHandlers = (socket: Socket) => {
  socket.on("setGame", async (data: ISetGameDTO, ack) => {
    let game = games.find(r => r.slug === data.slug);

    if (!game) {
      game = {
        id: gameNextId(),
        slug: data.slug,
        size: {
          x: DEFAULT_SIZE_X,
          y: DEFAULT_SIZE_Y
        },
        content: generateMap(DEFAULT_SIZE_X, DEFAULT_SIZE_Y),
        players: [
          {
            id: 123,
            model: 0,
            nickname: "TEst",
            alive: true,
            pos: {
              x: 0,
              y: 0
            }
          }
        ]
      };
      games.push(game);
    }

    ack(game);

    socket.request.context.gameId = game.id;
  });
  socket.on("getGame", ack => {
    if (!socket.request.context.gameId) {
      ack({
        code: ErrorCode.GAME_NOT_SELECTED,
        error: "Game not selected"
      } as IErrorDTO);
      return;
    }

    ack(games.find(r => r.id === socket.request.context.gameId) as IGameDTO);
  });
  socket.on("controlGame", async (data: ISetGameDTO, ack) => {
    const game = games.find(r => r.slug === data.slug);
    if (!game) {
      ack({
        code: ErrorCode.GAME_NOT_EXISTS,
        error: "Game not exists"
      } as IErrorDTO);
      return;
    }

    socket.request.context.controlGameId = game.id;
  });
  socket.on("act", (data: IActionDTO) => {
    console.log("action", data);
  });
};
