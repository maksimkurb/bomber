import { Socket } from "socket.io";

import { ISetNicknameDTO } from "../../shared/ISetNicknameDTO";

export const configHandlers = (socket: Socket) => {
  socket.on("setNickname", (data: ISetNicknameDTO) => {
    socket.request.session.nickname = data.nickname;
  });
  socket.on("whoami", () => {
    socket.emit("whoami", socket.request.session.nickname);
  });
};
