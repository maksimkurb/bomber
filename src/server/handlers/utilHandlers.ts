import { Socket } from "socket.io";

import { ISlugDTO } from "./../../shared/ISlugDTO.d";
import { randomWords } from "./../utils/randomWords";

export const utilHandlers = (socket: Socket) => {
  socket.on("slug", (ackFn: (ISlugDTO) => void) => {
    const slug: ISlugDTO = {
      slug: randomWords(3).join("-")
    };
    ackFn(slug);
  });
};
