import { IGameDTO } from "../../shared/IGameDTO";

const nextIdGenerator = () => {
  let id = 0;
  return () => {
    return id++;
  };
};
export const games: IGameDTO[] = [];
export const gameNextId = nextIdGenerator();
