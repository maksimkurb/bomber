import { ActionType } from "./ActionType";

export interface IActionDTO {
  game: number;
  type: ActionType;
}
