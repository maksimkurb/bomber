import { GameState } from "./GameState";
import { IPointLikeDTO } from "./IPointLikeDTO";
import { IPlayerDTO } from "./IPlayerDTO.d";

export interface IGameDTO {
  id: number;
  state?: GameState;
  size?: IPointLikeDTO;
  slug?: string;
  content?: number[][];
  players?: IPlayerDTO[];
}
