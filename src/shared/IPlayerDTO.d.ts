import { IPointLikeDTO } from "./IPointLikeDTO";

export interface IPlayerDTO {
  id: number;
  alive?: boolean;
  model?: number;
  nickname?: string;
  pos?: IPointLikeDTO;
}
