export enum WallType {
  BLOCK_NONE = 0,
  BLOCK_SOLID = 1,
  BLOCK_FRAGILE = 2,
  BLOCK_FORTIFIED = 3
}
