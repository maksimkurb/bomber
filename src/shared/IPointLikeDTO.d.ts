export interface IPointLikeDTO {
  x: number;
  y: number;
}
