import { ErrorCode } from "./ErrorCode";
export interface IErrorDTO {
  code: ErrorCode;
  error: string;
}
