export enum ActionType {
  MOVE_UP,
  MOVE_RIGHT,
  MOVE_BOTTOM,
  MOVE_LEFT,
  PLANT_BOMB
}
