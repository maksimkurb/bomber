import * as PIXI from "pixi.js";

const sprites = require("./sprites.json");
require("!file-loader?name=[name].[ext]!./sprites.png");

let texturesLoaded = false;
export const loadTextures = () =>
  new Promise(res => {
    if (texturesLoaded) {
      res();
    } else {
      PIXI.loader.add(sprites);
      PIXI.loader.load(res);
    }
  }).then(() => {
    texturesLoaded = true;
    return mapTextures();
  });

export const mapTextures = () => {
  const resources = PIXI.loader.resources[sprites];
  return {
    wall: {
      immutable: resources.textures["wall-immutable.png"],
      fortified: resources.textures["wall-fortified.png"],
      fragile: resources.textures["wall-fragile.png"]
    },

    bomb: resources.textures["bomb.png"],

    flame: [
      resources.textures["flame-0.png"],
      resources.textures["flame-1.png"],
      resources.textures["flame-2.png"]
    ],

    player: [0, 1, 2, 3].map(n => ({
      up: resources.textures[`player${n}-up.png`],
      right: resources.textures[`player${n}-right.png`],
      down: resources.textures[`player${n}-down.png`],
      left: resources.textures[`player${n}-left.png`]
    })),

    border: {
      // angle
      lt: resources.textures[`border-lt.png`],
      lb: resources.textures[`border-lb.png`],
      rt: resources.textures[`border-rt.png`],
      rb: resources.textures[`border-rb.png`],

      // side
      mt: resources.textures[`border-mt.png`],
      mr: resources.textures[`border-mr.png`],
      mb: resources.textures[`border-mb.png`],
      ml: resources.textures[`border-ml.png`]
    }
  };
};
