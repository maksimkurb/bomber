{
"frames": {
	"bomb.png": {
		"frame": {"x":65, "y":130, "w":56, "h":60},
		"spriteSourceSize": {"x":4,"y":0,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	},
	"border-lb.png": {
		"frame": {"x":131, "y":236, "w":16, "h":16},
		"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
		"sourceSize": {"w":16,"h":16}
	},
	"border-lt.png": {
		"frame": {"x":130, "y":106, "w":16, "h":16},
		"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
		"sourceSize": {"w":16,"h":16}
	},
	"border-mb.png": {
		"frame": {"x":155, "y":159, "w":5, "h":16},
		"spriteSourceSize": {"x":0,"y":0,"w":5,"h":16},
		"sourceSize": {"w":5,"h":16}
	},
	"border-ml.png": {
		"frame": {"x":0, "y":250, "w":16, "h":5},
		"spriteSourceSize": {"x":0,"y":0,"w":16,"h":5},
		"sourceSize": {"w":16,"h":5}
	},
	"border-mr.png": {
		"frame": {"x":0, "y":244, "w":16, "h":5},
		"spriteSourceSize": {"x":0,"y":0,"w":16,"h":5},
		"sourceSize": {"w":16,"h":5}
	},
	"border-mt.png": {
		"frame": {"x":147, "y":106, "w":5, "h":16},
		"spriteSourceSize": {"x":0,"y":0,"w":5,"h":16},
		"sourceSize": {"w":5,"h":16}
	},
	"border-rb.png": {
		"frame": {"x":148, "y":236, "w":16, "h":16},
		"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
		"sourceSize": {"w":16,"h":16}
	},
	"border-rt.png": {
		"frame": {"x":164, "y":212, "w":16, "h":16},
		"spriteSourceSize": {"x":0,"y":0,"w":16,"h":16},
		"sourceSize": {"w":16,"h":16}
	},
	"flame0.png": {
		"frame": {"x":0, "y":65, "w":64, "h":64},
		"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	},
	"flame1.png": {
		"frame": {"x":0, "y":130, "w":64, "h":56},
		"spriteSourceSize": {"x":0,"y":4,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	},
	"flame2.png": {
		"frame": {"x":0, "y":187, "w":64, "h":56},
		"spriteSourceSize": {"x":0,"y":4,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	},
	"player0-down.png": {
		"frame": {"x":226, "y":155, "w":30, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player0-left.png": {
		"frame": {"x":98, "y":191, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player0-right.png": {
		"frame": {"x":65, "y":191, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player0-up.png": {
		"frame": {"x":219, "y":104, "w":30, "h":50},
		"spriteSourceSize": {"x":0,"y":2,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player1-down.png": {
		"frame": {"x":196, "y":0, "w":30, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player1-left.png": {
		"frame": {"x":130, "y":0, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player1-right.png": {
		"frame": {"x":130, "y":53, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player1-up.png": {
		"frame": {"x":163, "y":53, "w":30, "h":50},
		"spriteSourceSize": {"x":0,"y":2,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player2-down.png": {
		"frame": {"x":164, "y":159, "w":30, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player2-left.png": {
		"frame": {"x":131, "y":183, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player2-right.png": {
		"frame": {"x":122, "y":130, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player2-up.png": {
		"frame": {"x":194, "y":53, "w":30, "h":50},
		"spriteSourceSize": {"x":0,"y":2,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player3-down.png": {
		"frame": {"x":188, "y":104, "w":30, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player3-left.png": {
		"frame": {"x":163, "y":0, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player3-right.png": {
		"frame": {"x":155, "y":106, "w":32, "h":52},
		"spriteSourceSize": {"x":0,"y":0,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"player3-up.png": {
		"frame": {"x":195, "y":157, "w":30, "h":50},
		"spriteSourceSize": {"x":0,"y":2,"w":32,"h":52},
		"sourceSize": {"w":32,"h":52}
	},
	"wall-fortified.png": {
		"frame": {"x":65, "y":0, "w":64, "h":64},
		"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	},
	"wall-fragile.png": {
		"frame": {"x":0, "y":0, "w":64, "h":64},
		"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	},
	"wall-immutable.png": {
		"frame": {"x":65, "y":65, "w":64, "h":64},
		"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
		"sourceSize": {"w":64,"h":64}
	}

},
"meta": {
	"image": "sprites.png",
	"size": {"w": 257, "h": 256},
	"scale": "1"
}
}