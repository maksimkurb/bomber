import {
  BLOCK_SOLID,
  BLOCK_FRAGILE,
  BLOCK_NONE,
} from './constants';

/* eslint-disable prefer-destructuring, no-param-reassign */

function deployAtDirection(pos, room, px, py) {
  const deadPlayers = [];
  for (let i = 0; i <= 2; i++) {
    const x = pos.x + (i * px);
    const y = pos.y + (i * py);
    if (
      // Overflow check
      x < 0 ||
      y < 0 ||
      x >= room.size.x ||
      y >= room.size.y ||
      // of if solid
      room.map[x][y] === BLOCK_SOLID
    ) break;
    if (room.map[x][y] === BLOCK_FRAGILE) {
      room.map[x][y] = BLOCK_NONE;
    }
    const deadPlIdx = room.players.findIndex(pl => pl.pos.x === x && pl.pos.y === y);
    if (deadPlIdx !== -1) {
      deadPlayers.push(deadPlIdx);
    }
  }
  return deadPlayers;
}
function deployBomb(pos, room) {
  let deadPlayers = [];

  deadPlayers = deadPlayers.concat(deployAtDirection(pos, room, 1, 0));
  deadPlayers = deadPlayers.concat(deployAtDirection(pos, room, -1, 0));
  deadPlayers = deadPlayers.concat(deployAtDirection(pos, room, 0, 1));
  deadPlayers = deadPlayers.concat(deployAtDirection(pos, room, 0, -1));
  return deadPlayers;
}

export default deployBomb;
