export const SCALE_FLAME = 1;
export const SCALE_PLAYER = 1.1;
export const SCALE_BLOCK = 0.95;
export const SCALE_BOMB = 0.6;
