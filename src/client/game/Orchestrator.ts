import * as PIXI from "pixi.js";
import { autorun } from "mobx";
import { Instance } from "mobx-state-tree";

import { GameStore } from "./../pages/game/store";
import { WallType } from "../../shared/WallType";
import { Player } from "../pages/game/store";
import { loadTextures, mapTextures } from "./textures";

import { SCALE_BLOCK, SCALE_PLAYER } from "./constants";

import animateBomb from "./animateBomb";

export const DIR_LEFT = 0;
export const DIR_UP = 1;
export const DIR_RIGHT = 2;
export const DIR_DOWN = 3;

export type Rect = {
  padding: number;
  x: number;
  y: number;
  w: number;
  h: number;
  blockSize: number;
};

type Entity = {
  sprite?: PIXI.Sprite;
};
type WallEntity = Entity & {
  type: WallType;
};
type PlayerEntity = Entity;
type BombEntity = Entity & {
  bombId: number;
};

export const calculateRect = (x, y, blockSize = 64): Rect => {
  const w = blockSize * x;
  const h = blockSize * y;

  return {
    padding: blockSize / 4,
    x,
    y,
    w,
    h,
    blockSize
  };
};

export class Orchestrator {
  private initialized = false;

  private walls: WallEntity[][] = [];
  private players: { [id: string]: PlayerEntity } = {};
  private entities: Entity[] = [];

  private gameContainer: PIXI.Container;
  private bordersContainer: PIXI.Container;
  private topmostContainer: PIXI.Container;
  private playersContainer: PIXI.Container;
  private plantedContainer: PIXI.Container;

  private store: Instance<typeof GameStore>;
  private rect: Rect;
  private pixi: PIXI.Application;
  private textures: ReturnType<typeof mapTextures>;

  constructor(store: Instance<typeof GameStore>, rect: Rect) {
    if (!store) {
      throw new Error("You must provide store");
    }
    this.store = store;

    this.rect = rect;
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

    this.pixi = new PIXI.Application({
      width: this.rect.w,
      height: this.rect.h,
      transparent: true
    });
    this.pixi.renderer.roundPixels = true;
    this.pixi.renderer.resolution = window.devicePixelRatio || 1;
    this.pixi.renderer.autoResize = false;
  }

  public async init() {
    this.textures = await loadTextures();

    this.gameContainer = new PIXI.Container();
    this.bordersContainer = new PIXI.Container();

    this.topmostContainer = new PIXI.Container();
    this.plantedContainer = new PIXI.Container();
    this.playersContainer = new PIXI.Container();

    this.pixi.stage.addChild(this.gameContainer);
    // this.pixi.stage.addChild(this.bordersContainer);

    this.gameContainer.addChild(this.bordersContainer);
    this.gameContainer.addChild(this.topmostContainer);
    this.gameContainer.addChild(this.plantedContainer);
    this.gameContainer.addChild(this.playersContainer);

    this.createBorders();
    this.initializeWalls();
    this.initializePlayers();

    this.initialized = true;
  }

  public getViewElement() {
    return this.pixi.view;
  }

  public resize(w, h) {
    const { padding } = this.rect;
    const origAspect = this.rect.w / this.rect.h;

    const gw = Math.min((h / this.rect.h) * this.rect.w, w) - padding * 2 - 2;
    const gh = gw / origAspect;
    const gScale = Math.min(gw / this.rect.w, gh / this.rect.h);

    this.gameContainer.position.set((w - gw) / 2, (h - gh) / 2);
    this.gameContainer.scale.set(gScale, gScale);

    this.pixi.renderer.resize(w, h);
  }

  public onDetonate(id) {
    /*
    const { bomb } = this.players[id];

    for (let i = 0; i < 10; i++) {
      let x;
      let y;
      if (i < 5) {
        x = bomb.pos.x + (i - 2);
        y = bomb.pos.y; // eslint-disable-line prefer-destructuring
      } else {
        if (i === 7) continue;
        x = bomb.pos.x; // eslint-disable-line prefer-destructuring
        y = bomb.pos.y + (i - 7);
      }
      if (
        // Overflow check
        x >= 0 &&
        y >= 0 &&
        x < this.rect.x &&
        y < this.rect.y &&
        // and if fragile
        this.room.map[x][y] === BLOCK_FRAGILE
      ) {
        this.objectsMap[x][y].remove();
        this.objectsMap[x][y] = null;
        this.room.map[x][y] = BLOCK_NONE;
      }
    }
    bomb.obj.remove();
    this.players[id].bomb = null;
    this.update();
    */
  }

  public size(x = 0, y = 0, w = 1, h = null) {
    if (h === null) {
      h = w;
    }
    const { blockSize } = this.rect;
    return {
      x: x * blockSize,
      y: y * blockSize,
      w: w * blockSize,
      h: h * blockSize
    };
  }

  public initializeWalls() {
    this.walls = [];
    for (let i = 0; i < this.rect.x; i++) {
      this.walls[i] = [];
      for (let j = 0; j < this.rect.x; j++) {
        this.walls[i][j] = null;
      }
    }

    const mapContent = this.store.map.content;
    for (let i = 0; i < this.rect.x; i++) {
      autorun(() => {
        const row = mapContent[i];
        for (let j = 0; j < this.rect.y; j++) {
          const wallType = row[j];
          const { x, y, w, h } = this.size(i, j, SCALE_BLOCK);

          let wall: WallEntity = this.walls[i][j];

          if (wallType === WallType.BLOCK_SOLID) {
            wall = {
              sprite: new PIXI.Sprite(this.textures.wall.immutable),
              type: WallType.BLOCK_SOLID
            };
          } else if (wallType === WallType.BLOCK_FRAGILE) {
            wall = {
              sprite: new PIXI.Sprite(this.textures.wall.fragile),
              type: WallType.BLOCK_FRAGILE
            };
          } else if (wallType === WallType.BLOCK_NONE) {
            if (wall) {
              this.plantedContainer.removeChild(wall.sprite);
            }
            wall = null;
          }
          if (wall) {
            wall.sprite.position.set(x, y);
            this.plantedContainer.addChild(wall.sprite);
          }

          this.walls[i][j] = wall;
        }
      });
    }
  }

  public initializePlayers() {
    const offsetX =
      (this.rect.blockSize - this.textures.player[0].down.width) / 2;
    const offsetY =
      (this.rect.blockSize - this.textures.player[0].down.height) / 2;
    autorun(() => {
      this.store.players.forEach(player => {
        const { x, y, w, h } = this.size(
          player.pos.x,
          player.pos.y,
          SCALE_PLAYER
        );
        const entity = {
          sprite: new PIXI.Sprite(this.textures.player[player.model].down)
        };
        entity.sprite.position.set(x + offsetX, y + offsetY);
        this.playersContainer.addChild(entity.sprite);
      });
    });
  }

  public update() {
    // this.pixi.update();
  }

  public onMount() {
    this.update();
  }

  public reset() {
    this.walls = [];
    this.players = {};
    this.entities = [];

    this.pixi.stage.removeChild(this.gameContainer);
    this.pixi.stage.removeChild(this.bordersContainer);
    this.gameContainer = null;
    this.bordersContainer = null;

    this.topmostContainer = null;
    this.playersContainer = null;
    this.plantedContainer = null;
  }

  public onUnmount = () => {
    // stub
  };

  private createBorders() {
    const { padding } = this.rect;
    const parts = [
      ["mt", 0, -padding, this.rect.w, padding],
      ["mr", this.rect.w, 0, padding, this.rect.h],
      ["mb", 0, this.rect.h, this.rect.w, padding],
      ["ml", -padding, 0, padding, this.rect.h],
      ["lt", -padding, -padding, padding, padding],
      ["lb", -padding, this.rect.h, padding, padding],
      ["rt", this.rect.w, -padding, padding, padding],
      ["rb", this.rect.w, this.rect.h, padding, padding]
    ];

    parts.forEach(
      ([tex, x, y, w, h]: [string, number, number, number, number]) => {
        const sprite = new PIXI.Sprite(this.textures.border[tex]);
        sprite.width = w;
        sprite.height = h;
        sprite.position.set(x, y);
        this.bordersContainer.addChild(sprite);
      }
    );
  }
}
