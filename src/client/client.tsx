import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "./components/App/App";

import "./socket";

ReactDOM.render(<App />, document.getElementById("app"));
