import {
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
  withStyles
} from "@material-ui/core";
import { observer } from "mobx-react";
import * as React from "react";
import { Link } from "react-router-dom";

import { Logo } from "../../components/Logo";

import { store } from "./store";
import { styles } from "./RoomSelection.styles";

store.setRandomSlug();

const handleSlugChange = e => {
  const slug = e.target.value
    .toLowerCase()
    .replace(new RegExp("\\s", "g"), "-")
    .replace(new RegExp("[^a-zа-я0-9-.ё]", "g"), "");
  store.setSlug(slug);
};

const RoomSelectionComponent = ({ classes }) => (
  <Grid
    container
    alignItems="center"
    direction="column"
    className={classes.root}
  >
    <Grid item>
      <Logo variant="huge" className={classes.logo} />
    </Grid>
    <Grid item>
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="headline">
            Создайте комнату, чтобы играть
          </Typography>
          <Grid container spacing={16} className={classes.form}>
            <Grid item xs={8}>
              <TextField
                inputProps={{
                  className: classes.inputField
                }}
                type="text"
                fullWidth
                placeholder="Название комнаты"
                value={store.slug}
                onChange={handleSlugChange}
              />
            </Grid>
            <Grid item xs={4}>
              <Button
                fullWidth
                color="primary"
                variant="raised"
                className={classes.button}
                component={(p: any) => (
                  <Link to={`/game/${store.slug}`} {...p} />
                )}
              >
                Начать!
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  </Grid>
);
export const RoomSelection = withStyles(styles)(
  observer(RoomSelectionComponent)
);
