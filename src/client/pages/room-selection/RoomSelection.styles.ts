import { StyleRules } from "@material-ui/core/styles";

export const styles: StyleRules = {
  "@keyframes appear": {
    from: { opacity: 0, transform: "translateY(-0.2em)" },
    to: { opacity: 1 }
  },
  root: {
    justifyContent: "center"
  },
  logo: {
    animation: "appear 0.8s"
  },
  form: {
    marginTop: "1em"
  },
  inputField: {
    height: "2em",
    padding: ".5em"
  },
  button: {
    height: "3.5em",
    padding: "1em 3em"
  }
};
