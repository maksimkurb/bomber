import * as React from "react";
import { RouteComponentProps } from "react-router";
import { Grid, withStyles, WithStyles } from "@material-ui/core";
import { observer } from "mobx-react";

import { vibrate } from "../../utils/vibrate";
import { store } from "./store";
import { playerTextures } from "./playerTextures";
import { styles } from "./Controller.styles";
import { Button } from "../../components/Button/Button";

interface IControllerParams {
  slug: string;
}
type ControllerProps = WithStyles<typeof styles> &
  RouteComponentProps<IControllerParams>;

class ControllerComponent extends React.Component<ControllerProps> {
  public handleButtonMouseDown = e => {
    e.preventDefault();
    vibrate(80);
    e.currentTarget.classList.add("clicked");
    /*
    Meteor.call(
      "room.handleAction",
      this.props.room._id,
      getSession(),
      e.currentTarget.dataset.act,
      (err, res) => {
        console.log(err, res);
      }
    );*/
  };
  public handleButtonMouseUp = e => {
    e.preventDefault();
    e.currentTarget.classList.remove("clicked");
  };

  public render() {
    const { classes } = this.props;

    const touchEvents = {
      onTouchStart: this.handleButtonMouseDown,
      onTouchEnd: this.handleButtonMouseUp,
      onMouseDown: this.handleButtonMouseDown,
      onMouseUp: this.handleButtonMouseUp,
      onMouseLeave: this.handleButtonMouseUp
    };

    return (
      <Grid
        container
        alignItems="center"
        direction="row"
        className={classes.root}
      >
        <Grid item xs={5} className={classes.buttonContainer}>
          <Button className={classes.button} data-act="bomb" {...touchEvents}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
              <path d="M571 589q-10-25-34-35t-49 0q-108 44-191 127t-127 191q-10 25 0 49t35 34q13 5 24 5 42 0 60-40 34-84 98.5-148.5t148.5-98.5q25-11 35-35t0-49zm942-356l46 46-244 243 68 68q19 19 19 45.5t-19 45.5l-64 64q89 161 89 343 0 143-55.5 273.5t-150 225-225 150-273.5 55.5-273.5-55.5-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5q182 0 343 89l64-64q19-19 45.5-19t45.5 19l68 68zm8-56q-10 10-22 10-13 0-23-10l-91-90q-9-10-9-23t9-23q10-9 23-9t23 9l90 91q10 9 10 22.5t-10 22.5zm230 230q-11 9-23 9t-23-9l-90-91q-10-9-10-22.5t10-22.5q9-10 22.5-10t22.5 10l91 90q9 10 9 23t-9 23zm41-183q0 14-9 23t-23 9h-96q-14 0-23-9t-9-23 9-23 23-9h96q14 0 23 9t9 23zm-192-192v96q0 14-9 23t-23 9-23-9-9-23v-96q0-14 9-23t23-9 23 9 9 23zm151 55l-91 90q-10 10-22 10-13 0-23-10-10-9-10-22.5t10-22.5l90-91q10-9 23-9t23 9q9 10 9 23t-9 23z" />
            </svg>
          </Button>
        </Grid>
        <Grid item xs={2} className={classes.alignCenter}>
          <div>
            <img src={playerTextures[0]} alt="Вы" />
            <h5>ВЫ</h5>
          </div>
        </Grid>
        <Grid item xs={5} className={classes.buttonContainer}>
          <Button className={classes.button} data-act="up" {...touchEvents}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 451.847 451.846"
            >
              <path d="M248.292,106.406l194.281,194.29c12.365,12.359,12.365,32.391,0,44.744c-12.354,12.354-32.391,12.354-44.744,0 L225.923,173.529L54.018,345.44c-12.36,12.354-32.395,12.354-44.748,0c-12.359-12.354-12.359-32.391,0-44.75L203.554,106.4 c6.18-6.174,14.271-9.259,22.369-9.259C234.018,97.141,242.115,100.232,248.292,106.406z" />
            </svg>
          </Button>
          <Button className={classes.button} data-act="right" {...touchEvents}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 451.846 451.847"
            >
              <path d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744 L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284 c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z" />
            </svg>
          </Button>
          <Button className={classes.button} data-act="down" {...touchEvents}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 451.847 451.847"
            >
              <path d="M225.923,354.706c-8.098,0-16.195-3.092-22.369-9.263L9.27,151.157c-12.359-12.359-12.359-32.397,0-44.751 c12.354-12.354,32.388-12.354,44.748,0l171.905,171.915l171.906-171.909c12.359-12.354,32.391-12.354,44.744,0 c12.365,12.354,12.365,32.392,0,44.751L248.292,345.449C242.115,351.621,234.018,354.706,225.923,354.706z" />
            </svg>
          </Button>
          <Button className={classes.button} data-act="left" {...touchEvents}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 451.847 451.847"
            >
              <path d="M97.141,225.92c0-8.095,3.091-16.192,9.259-22.366L300.689,9.27c12.359-12.359,32.397-12.359,44.751,0 c12.354,12.354,12.354,32.388,0,44.74L173.525,225.92l171.903,171.909c12.354,12.354,12.354,32.391,0,44.744 c-12.354,12.365-32.386,12.365-44.745,0l-194.29-194.281C100.226,242.115,97.141,234.018,97.141,225.92z" />
            </svg>
          </Button>
        </Grid>
      </Grid>
    );
  }
}
export const Controller = withStyles(styles)(observer(ControllerComponent));
