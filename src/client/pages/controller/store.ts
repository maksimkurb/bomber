import { flow, types } from "mobx-state-tree";

import socket from "../../socket";

export const RoomSelectionStore = types
  .model("RoomSelectionStore", {
    loading: types.boolean,
    slug: types.string
  })
  .actions(self => ({
    setRandomSlug: flow(function*() {
      self.loading = true;
      const slug = yield new Promise((resolve, reject) => {
        try {
          socket.emit("slug", res => {
            resolve(res.slug);
          });
        } catch (e) {
          reject(e);
        }
      });

      self.slug = slug;
      self.loading = false;
    }),
    setSlug: slug => (self.slug = slug)
  }));

export const store = RoomSelectionStore.create({
  loading: false,
  slug: ""
});
