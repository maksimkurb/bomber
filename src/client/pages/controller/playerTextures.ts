export const playerTextures = [0, 1, 2, 3].map(id =>
  require(`../../game/textures/originals/player${id}-down.png`)
);
