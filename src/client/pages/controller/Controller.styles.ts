const buttonSize = 80;
export const styles = {
  root: {},
  buttonContainer: {
    position: "relative" as "relative",
    width: buttonSize * 3,
    height: buttonSize * 3,
    "& button": {
      position: "absolute" as "absolute"
    },
    "& button[data-act='up']": {
      top: 0,
      right: buttonSize
    },
    "& button[data-act='right']": {
      top: buttonSize,
      right: 0
    },
    "& button[data-act='down']": {
      top: buttonSize * 2,
      right: buttonSize
    },
    "& button[data-act='left']": {
      top: buttonSize,
      right: buttonSize * 2
    },
    "& button[data-act='bomb']": {
      width: buttonSize * 2,
      height: buttonSize * 2,
      "margin-top": buttonSize / 2
    }
  },
  alignCenter: {
    "text-align": "center"
  },
  button: {
    height: buttonSize,
    width: buttonSize,
    "font-size": "2rem",
    "& svg": {
      width: "2rem",
      height: "2rem",
      fill: "white"
    }
  }
};
