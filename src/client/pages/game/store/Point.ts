import { types } from "mobx-state-tree";

export const Point = types
  .model("Point", {
    x: types.optional(types.number, 0),
    y: types.optional(types.number, 0)
  })
  .actions(self => ({
    setValue: (x, y) => {
      self.x = x;
      self.y = y;
    },
    setX: x => {
      self.x = x;
    },
    setY: y => {
      self.y = y;
    }
  }));
