import { types } from "mobx-state-tree";

import { IPlayerDTO } from "./../../../../shared/IPlayerDTO.d";
import { Point } from "./Point";

export const Player = types
  .model("Player", {
    id: types.number,
    model: types.number,
    nickname: types.string,
    alive: types.boolean,
    pos: Point
  })
  .actions(self => ({
    setNickname: (nickname: string) => {
      self.nickname = nickname;
    },
    setAlive: (alive: boolean) => {
      self.alive = alive;
    },
    setPos: (x: number, y: number) => {
      if (!self.pos) {
        self.pos = Point.create();
      }
      self.pos.setValue(x, y);
    },
    update: (playerInfo: IPlayerDTO) => {
      if (playerInfo.hasOwnProperty("model")) {
        self.model = playerInfo.model;
      }
      if (playerInfo.hasOwnProperty("nickname")) {
        self.nickname = playerInfo.nickname;
      }
      if (playerInfo.hasOwnProperty("alive")) {
        self.alive = playerInfo.alive;
      }
      if (playerInfo.hasOwnProperty("pos")) {
        self.pos.setValue(playerInfo.pos.x, playerInfo.pos.y);
      }
    }
  }));
