import { types } from "mobx-state-tree";
import { $enum } from "ts-enum-util";

import { GameState } from "./../../../../shared/GameState";
import { IGameDTO } from "../../../../shared/IGameDTO";

import { Point } from "./Point";
import { Player } from "./Player";
import { Map } from "./Map";

export const GameStore = types
  .model("GameStore", {
    slug: types.string,
    state: types.enumeration<GameState>($enum(GameState).getValues()),
    map: Map,
    players: types.array(Player)
  })
  .actions(self => ({
    reset: () => {
      self.slug = "";
      self.map.clear();
      self.players.clear();
    },
    setSlug: slug => (self.slug = slug),
    getPlayer: id => {
      return self.players.find(pl => pl.id === id);
    },
    update: (game: IGameDTO) => {
      if (game.size) {
        self.map.setSize(game.size.x, game.size.y);
      }
      if (game.content) {
        self.map.setContent(game.content);
      }
      if (game.players) {
        game.players.forEach(playerInfo => {
          const player = self.players.find(p => p.id === playerInfo.id);
          if (player) {
            player.update(playerInfo);
          } else {
            self.players.push(
              Player.create({
                id: playerInfo.id,
                alive: playerInfo.alive || true,
                model: playerInfo.model || 0,
                nickname: playerInfo.nickname || "unnamed",
                pos: Point.create({
                  ...playerInfo.pos
                })
              })
            );
          }
        });
      }
    }
  }));

export const store = GameStore.create({
  slug: "",
  state: GameState.WAITING_FOR_PLAYERS,
  map: Map.create({
    size: {},
    content: []
  }),
  players: []
});

export { Player, Map, Point };
