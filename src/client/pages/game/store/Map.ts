import { IMSTArray, types } from "mobx-state-tree";

import { Point } from "./Point";

export const Map = types
  .model("Map", {
    size: Point,
    content: types.array(types.array(types.number))
  })
  .actions(self => ({
    clear: () => {
      self.size = Point.create();
      self.content.clear();
    },
    setSize: (x, y) => {
      self.size.setValue(x, y);
    },
    setContent: (content: number[][]) => {
      self.content.replace(content as Array<IMSTArray<number, number, number>>);
    }
  }));
