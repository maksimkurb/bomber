import { StyleRules } from "@material-ui/core/styles";

export const styles: StyleRules = {
  root: {
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  gameContainer: {
    flex: 1,
    padding: "1rem",
    position: "relative"
  },
  game: {
    height: "100%",
    overflow: "hidden",
    position: "relative",

    "& canvas": {
      position: "absolute",
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    }
  }
};
