import { Grid, withStyles, WithStyles } from "@material-ui/core";
import { RouteComponentProps } from "react-router";
import { observer } from "mobx-react";
import * as React from "react";

import { WaitingForPlayers } from "../../components/WaitingForPlayers";

import { IGameDTO } from "../../../shared/IGameDTO";
import { ISetGameDTO } from "../../../shared/ISetGameDTO";
import { GameState } from "../../../shared/GameState";

import socket from "../../socket";
import { calculateRect, Orchestrator, Rect } from "../../game/Orchestrator";

import { Player, Point, store } from "./store";
import { styles } from "./Game.styles";
import { Header } from "../../components/Header/Header";

interface IRouterParams {
  slug: string;
}
type GameProps = WithStyles<typeof styles> & RouteComponentProps<IRouterParams>;

@observer
class GameComponent extends React.Component<GameProps> {
  private gameContainer: HTMLElement;
  private orchestrator: Orchestrator;
  private rect: Rect;

  constructor(props) {
    super(props);
  }

  public componentWillMount() {
    store.reset();
    store.setSlug(this.props.match.params.slug);
  }

  public componentDidMount() {
    socket.emit(
      "setGame",
      {
        slug: this.props.match.params.slug
      } as ISetGameDTO,
      async (gameInfo: IGameDTO) => {
        this.updateStore(gameInfo);

        this.rect = calculateRect(gameInfo.size.x, gameInfo.size.y);

        this.orchestrator = new Orchestrator(store, this.rect);
        await this.orchestrator.init();
        const view = this.orchestrator.getViewElement();
        this.gameContainer.appendChild(view);

        window.addEventListener("resize", this.onResize);
        this.onResize();
      }
    );
  }

  public componentWillUnmount() {
    if (this.orchestrator) {
      this.orchestrator.onUnmount();
    }
    window.removeEventListener("resize", this.onResize);
  }

  public render() {
    const { classes } = this.props;
    return (
      <Grid item xs={12} className={classes.root}>
        <Header />
        <div className={classes.gameContainer}>
          <div className={classes.game} ref={el => (this.gameContainer = el)} />
          {store.state === GameState.WAITING_FOR_PLAYERS && (
            <WaitingForPlayers />
          )}
        </div>
      </Grid>
    );
  }

  private updateStore(gameInfo: IGameDTO) {
    store.update(gameInfo);
  }

  private onResize = () => {
    this.orchestrator.resize(
      this.gameContainer.clientWidth,
      this.gameContainer.clientHeight
    );
  };
}
export const Game = withStyles(styles)(GameComponent);
