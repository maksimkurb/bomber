import { StyleRules } from "@material-ui/core/styles";
import { transparentize } from "polished";

import { palette } from "../../theme";

const background = [palette.secondary["100"], palette.secondary["200"]]
  .map(transparentize(0.2))
  .join(", ");

export const styles: StyleRules = {
  root: {
    backgroundImage: `linear-gradient(to top, ${background})`,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,

    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
};
