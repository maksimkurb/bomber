import * as React from "react";
import { withStyles } from "@material-ui/core";
import cx from "classnames";

import { styles } from "./Overlay.styles";

const OverlayComponent = ({ classes, children, ...rest }) => (
  <div {...rest} className={cx(classes.root, rest.className)}>
    {children}
  </div>
);

export const Overlay = withStyles(styles)(OverlayComponent);
