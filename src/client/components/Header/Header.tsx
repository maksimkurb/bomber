import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  withStyles
} from "@material-ui/core";
import * as React from "react";
import { Link } from "react-router-dom";
import cx from "classnames";

import { styles } from "./Header.styles";

export const HeaderComponent = ({ classes, ...rest }) => (
  <AppBar
    position="static"
    color="default"
    className={cx(classes.root, rest.className)}
  >
    <Toolbar variant="dense">
      <Typography variant="title" color="inherit" className={classes.logo}>
        Bomber
      </Typography>
      <Button color="primary" component={(p: any) => <Link to="/" {...p} />}>
        Home
      </Button>
      <Button
        color="primary"
        component={(p: any) => <Link to="/users-list" {...p} />}
      >
        Example Users List
      </Button>
    </Toolbar>
  </AppBar>
);

export const Header = withStyles(styles)(HeaderComponent);
