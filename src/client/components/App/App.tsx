import JssProvider from "react-jss/lib/JssProvider";
import { create } from "jss";
import jssGlobal from "jss-global";
import jssCamelCase from "jss-camel-case";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createGenerateClassName, jssPreset } from "@material-ui/core/styles";
import { MuiThemeProvider, withStyles } from "@material-ui/core";
import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { Header } from "../Header/Header";
import { styles } from "./App.styles";

// Pages
import { RoomSelection } from "../../pages/room-selection";
import { Game } from "../../pages/game";
import { Controller } from "../../pages/controller";
import { muiTheme } from "../../theme";

let DevTools;
if (process.env.NODE_ENV === "development") {
  DevTools = require("mobx-react-devtools").default;
}

const generateClassName = createGenerateClassName();
const jss = create({
  ...jssPreset(),
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
  insertionPoint: "jss-insertion-point"
});

jss.use(jssCamelCase(), jssGlobal());

const AppComponent = ({ classes }) => (
  <JssProvider jss={jss} generateClassName={generateClassName}>
    <MuiThemeProvider theme={muiTheme}>
      <BrowserRouter>
        <div className={classes.root}>
          {process.env.NODE_ENV === "development" && <DevTools />}
          <CssBaseline />
          <div className={classes.content}>
            <Switch>
              <Route exact path="/" component={RoomSelection} />
              <Route path="/game/:slug" component={Game} />
              <Route path="/_/:slug" component={Controller} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    </MuiThemeProvider>
  </JssProvider>
);

export const App = withStyles(styles)(AppComponent);
