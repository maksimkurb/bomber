import { StyleRules } from "@material-ui/core/styles";
import { lighten } from "polished";

import { palette } from "../../theme";

const background = [palette.secondary["100"], palette.secondary["200"]].join(
  ", "
);

export const styles: StyleRules = {
  "@global": {
    "*": {
      boxSizing: "border-box"
    },
    html: {
      height: "100%",
      width: "100%"
    },

    body: {
      minHeight: "100%",
      width: "100%"
    },

    ".container": {
      height: "100vh",
      width: "100%",
      display: "flex"
    },
    "#app": {
      flex: 1
    }
  },
  root: {
    height: "100%",
    flex: 1,
    display: "flex",
    flexDirection: "column",
    background: palette.secondary["100"],
    backgroundImage: `linear-gradient(to left bottom, ${background});`
  },
  content: {
    flex: 1,
    display: "flex"
  }
};
