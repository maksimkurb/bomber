import { StyleRules } from "@material-ui/core/styles";

import { palette } from "../../theme";

export const styles: StyleRules = {
  content: {},
  stroke: {
    textDecoration: "line-through"
  },
  pad: {
    marginTop: "1em"
  },
  phone: {
    padding: "78px 19px 74px 21px",
    width: 250,
    height: 524,
    backgroundImage: `url(${require("./phone.png")})`,
    backgroundSize: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",

    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",

    "& > h4": {
      margin: 0
    }
  },
  help: {
    display: "flex",
    alignItems: "center",
    padding: "1.5em"
  }
};
