import * as React from "react";
import {
  Card,
  CardContent,
  Grid,
  Input,
  Typography,
  withStyles,
  WithStyles
} from "@material-ui/core";
import * as QRCode from "qrcode";

import { MAX_PLAYERS } from "../../../shared/constants";
import { store } from "../../pages/game/store";
import { Overlay } from "../Overlay";
import { styles } from "./WaitingForPlayer.styles";

type Props = WithStyles<typeof styles> & {
  className?: any;
};

class WaitingForPlayersComponent extends React.Component<Props> {
  private qrCanvas: HTMLCanvasElement;

  public componentDidMount() {
    const url = [window.location.origin, "_", store.slug].join("/");
    QRCode.toCanvas(
      this.qrCanvas,
      url,
      {
        scale: 5,
        color: {
          dark: "#02190d",
          light: "#fffff7"
        }
      },
      error => {
        if (error) {
          alert("Ошибка создания QR-кода. Смотри консоль");
          console.error(error);
        }
      }
    );
  }

  public render() {
    const { classes, ...rest } = this.props;
    return (
      <Overlay className={rest.className}>
        <Grid container direction="row" justify="center">
          <Grid item>
            <div className={classes.phone}>
              <Typography variant="title" component="h4">
                Просканируй меня!
              </Typography>
              <canvas
                ref={ref => {
                  this.qrCanvas = ref;
                }}
              />
            </div>
          </Grid>
          <Grid item xs={8} md={7} lg={6} className={classes.help}>
            <Card>
              <CardContent className={classes.content}>
                <Typography color="textSecondary">
                  Ожидание игроков...
                </Typography>

                <Typography className={classes.pad}>
                  Для начала игры необходимо зайти в комнату со своего
                  смартфона, который будет использоваться в качестве джойстика.
                </Typography>
                <Typography>
                  Просканируйте <strong>QR-код</strong> слева и{" "}
                  <span className={classes.stroke}>заставьте</span> предложите
                  другу сделать то же самое.
                </Typography>

                <Typography className={classes.pad}>
                  Если вы далеко друг от друга - тоже не беда. Отправьте ему эту
                  ссылку:
                </Typography>
                <Input
                  inputProps={{
                    onClick: this.onRoomUrlClick
                  }}
                  value={this.getRoomUrl()}
                  fullWidth
                  readOnly
                />
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Overlay>
    );
  }

  private getRoomUrl() {
    return window.location.href;
  }

  private onRoomUrlClick(e) {
    e.currentTarget.setSelectionRange(0, e.currentTarget.value.length);
  }
}

export const WaitingForPlayers = withStyles(styles)(WaitingForPlayersComponent);
