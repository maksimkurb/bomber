import * as React from "react";
import cx from "classnames";
import { withStyles, WithStyles } from "@material-ui/core/styles";

import { styles } from "./Logo.styles";

type LogoProps = WithStyles<typeof styles> & {
  variant: "default" | "huge";
  className?: any;
};

export const LogoComponent: React.SFC<LogoProps> = ({
  classes,
  variant = "default",
  className
}) => (
  <span
    className={cx(className, classes.logo, {
      [classes.huge]: variant === "huge"
    })}
  >
    Bomber
  </span>
);

export const Logo = withStyles(styles)(LogoComponent);
