import { StyleRules } from "@material-ui/core/styles";

import { palette } from "../../theme";

export const styles: StyleRules = {
  logo: {
    display: "block",
    textTransform: "uppercase",
    fontFamily: `"Nunito", sans-serif`,
    textShadow: "rgba(0,0,0,0.1) 0.04em 0.04em 0",
    color: palette.primary["500"]
  },
  huge: {
    fontSize: "7rem",
    textAlign: "center"
  }
};
