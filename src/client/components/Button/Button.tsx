import * as React from "react";
import cx from "classnames";
import { Button as MaterialButton, withStyles } from "@material-ui/core";

import { styles } from "./Button.styles";

const ButtonUstyled = ({ classes, className, ...props }) => (
  <MaterialButton variant="raised" color="secondary" {...props} />
);

export const Button = withStyles(styles)(ButtonUstyled);
