export const vibrate = window.navigator.vibrate
  ? time => {
      window.navigator.vibrate(time);
    }
  : () => {
      return;
    };
