const path = require("path");
const webpack = require("webpack");
const version = require("../../package.json").version;
const HtmlWebpackPlugin = require("html-webpack-plugin");

const config = require("../server/config");
const projectRoot = path.join(__dirname, "..", "..");

const plugins = [
  new HtmlWebpackPlugin({
    title: "Bomberman",
    favicon: "favicon.ico",
    filename: "index.html",
    template: "index.ejs"
  })
];

module.exports = {
  mode: config.IS_PRODUCTION ? "production" : "development",
  devtool: config.IS_PRODUCTION ? "" : "inline-source-map",
  entry: ["babel-polyfill", "./client"],
  output: {
    path: path.join(projectRoot, "dist", "public"),
    filename: `[name]-${version}-bundle.js`,
    publicPath: "/public/"
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx"]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "awesome-typescript-loader"
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              camelCase: true,
              sourceMap: !config.IS_PRODUCTION,
              minimize: config.IS_PRODUCTION
            }
          }
        ]
      },
      {
        test: /.jpe?g$|.gif$|.png$|.svg$|.woff$|.woff2$|.ttf$|.eot$/,
        use: "url-loader?limit=10000"
      },
      {
        test: /.json$/,
        exclude: /(node_modules|bower_components)/,
        type: "javascript/auto",
        use: [
          {
            loader: "file-loader",
            options: { name: "[name].[ext]" }
          }
        ]
      }
    ]
  },
  plugins
};
