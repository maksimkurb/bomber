import { darken } from "polished";
import yellow from "@material-ui/core/colors/yellow";
import green from "@material-ui/core/colors/green";
import { createMuiTheme } from "@material-ui/core";

export const palette = {
  primary: green,
  secondary: yellow
};

export const muiTheme = createMuiTheme({
  overrides: {
    MuiButton: {
      raisedPrimary: {
        backgroundImage: `linear-gradient(45deg, ${[
          palette.primary["500"],
          palette.primary["300"]
        ].join(",")})`
      }
    }
  },
  palette
});
