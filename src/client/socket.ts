import * as SocketIO from "socket.io-client";
import { Socket } from "socket.io";

const socket: Socket = SocketIO();
export default socket;

(window as any).socket = socket;
